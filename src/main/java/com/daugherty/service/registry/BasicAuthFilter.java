package com.daugherty.service.registry;

import java.util.Base64;
import java.util.Base64.Encoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class BasicAuthFilter extends ZuulFilter {

    private static final Logger log = LoggerFactory.getLogger(BasicAuthFilter.class);

    private final Base64.Encoder encoder;
    private final String domainMicroserviceUsername;
    private final String domainMicroservicePassword;

    public BasicAuthFilter(Encoder encoder, String domainMicroserviceUsername, String domainMicroservicePassword) {
        this.encoder = encoder;
        this.domainMicroserviceUsername = domainMicroserviceUsername;
        this.domainMicroservicePassword = domainMicroservicePassword;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public String filterType() {
        return "route";
    }

    @Override
    public Object run() {
        String auth = domainMicroserviceUsername + ":" + domainMicroservicePassword;
        String base64Auth = encoder.encodeToString(auth.getBytes());
        String basicAuthHeaderValue = "Basic " + base64Auth;
        log.info("Adding Authorization header: " + basicAuthHeaderValue);

        RequestContext ctx = RequestContext.getCurrentContext();
        ctx.addZuulRequestHeader("Authorization", basicAuthHeaderValue);

        return null;
    }

}
