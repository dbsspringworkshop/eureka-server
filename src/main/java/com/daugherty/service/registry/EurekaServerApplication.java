package com.daugherty.service.registry;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Bean;

import de.codecentric.boot.admin.config.EnableAdminServer;

@SpringBootApplication
@EnableEurekaServer
@EnableAdminServer
public class EurekaServerApplication {

    @Value("${domain.microservice.username}")
    private String domainMicroserviceUsername;
    @Value("${domain.microservice.password}")
    private String domainMicroservicePassword;

    public static void main(String[] args) {
        SpringApplication.run(EurekaServerApplication.class, args);
    }

    @Bean
    public BasicAuthFilter basicAuthFilter() {
        return new BasicAuthFilter(Base64.getEncoder(), domainMicroserviceUsername, domainMicroservicePassword);
    }
}
